import UIKit
import WebKit


class RequestTypeViewCell: UITableViewCell, UIWebViewDelegate {
    @IBOutlet weak var RequestTypeImage: WKWebView!
    @IBOutlet weak var RequestTypeType: UILabel!
    @IBOutlet weak var RequestTypeName: UILabel!
}


class RequestTypeController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var project :ProjectModel?
    var urlString: String = Constants.BaseUrl+Constants.RequestTypes
    var lista = [RequestTypeModel]()
    
    @IBOutlet var RequestTypeTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeUrl()
        requestDataFromServer()
        defineTable()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celd = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath)
        let celda = celd as! RequestTypeViewCell
        let item = lista[indexPath.item]
        celda.RequestTypeName?.text = item.Name
        celda.RequestTypeType?.text = item.Description
        //celda.RequestTypeImage?.load(URL(string:item.Icon!))
        
        
        let request: URLRequest = URLRequest(url: URL(string: item.Icon!)!)
        celda.RequestTypeImage.backgroundColor =  UIColor.clear
        celda.RequestTypeImage.load(request)
        
        
        var html = "<html><body><img style='width: 190px;height: 190px;position: fixed;' src='//url//'></body></html>"
        html = html.replacingOccurrences(of: "//url//", with: item.Icon!)
            celda.RequestTypeImage.loadHTMLString(html, baseURL: nil)
        
        return celda
    }
    func makeUrl(){
        urlString = urlString.replacingOccurrences(of: "{serviceDeskId}", with: project!.Id!)
    }
    
    private func requestDataFromServer(){
        var running = false
        let session = RequestHelper.generateSession()
        
        let task = session.dataTask(with: URL(string: urlString)!) {
            (data, response, error) in
            self.getObjectsFromResponse(response, data)
            running = false
        }
        
        running = true
        task.resume()
        
        while running {
            print("waiting...")
            sleep(1)
        }
        self.RequestTypeTable.reloadData()
    }
    
    
    // function from delegate protocol
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        let contentSize = webView.scrollView.contentSize
        let webViewSize = webView.bounds.size
        let scaleFactor = webViewSize.width / contentSize.width
        
        // scale the svg appropriately
        webView.scrollView.minimumZoomScale = scaleFactor
        webView.scrollView.maximumZoomScale = scaleFactor
        webView.scrollView.zoomScale = scaleFactor
    }
    
    private func MapObject(_ item:Any)->RequestTypeModel!{
        let obj = item as! [String: Any]
        let result = RequestTypeModel()
        let icons = (obj["icon"] as! [String: Any])
        let links = (icons["_links"]) as! [String: Any]
        let urls = (links["iconUrls"])as! [String: Any]
        result.Id = obj["id"] as? String
        result.Name = obj["name"] as? String
        result.Description = obj["description"] as? String
        result.HelpText =  obj["helpText"] as? String
        result.Icon =  urls["16x16"] as? String
        return result
    }
    
    private func loadImage(stringUrl:String)-> UIImage{
        var image:UIImage?
        do{
            let url = URL(string: stringUrl)
            let data = try Data(contentsOf: url!)
            image = UIImage(data : data)!
        }
        catch {
            print("Error")
        }
        return image!
    }
        
    
    private func getObjectsFromResponse(_ response: URLResponse?, _ data: Data?) {
        let json = RequestHelper.serializeJsonObjectWith(data: data!)
        let values = json["values"] as! [Any?]
        for item in values{
            self.lista.append(MapObject(item!))
        }
        print(lista)
    }
    
    private func defineTable(){
        RequestTypeTable.dataSource = self
        RequestTypeTable.delegate = self
    }
}

