import UIKit
import Foundation

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tabla: UITableView!
    var lista = [ProjectModel]()
    var segueToDetail = "ShowDetailSegue"
    var url = Constants.BaseUrl + Constants.ServiceDesksProjects
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestDataFromServer()
        defineTable()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case segueToDetail?:
            let recibido = sender as! Int
            let obj:RequestTypeController = segue.destination as! RequestTypeController
            obj.project = lista[recibido]
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celda = UITableViewCell()
        celda.textLabel!.text = lista[indexPath.row].ProjectName
        return celda
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pdfId = indexPath.row
        self.performSegue(withIdentifier: segueToDetail, sender: pdfId)
    }
    
    
    private func requestDataFromServer(){
        var running = false
        let session = RequestHelper.generateSession()
        print(self.url)
        let task = session.dataTask(with: URL(string: self.url)!) {
            (data, response, error) in
            self.getObjectsFromResponse(response, data)
            running = false
        }
        
        running = true
        task.resume()
        
        while running {
            print("waiting...")
            sleep(1)
        }
    }
    
    private func MapObject(_ item:Any)->ProjectModel!{
        let obj = item as! [String: Any]
        let result = ProjectModel()
        result.Id = obj["id"] as? String
        result.ProjectId = obj["projectId"] as? String
        result.ProjectName = obj["projectName"] as? String
        result.ProjectKey =  obj["projectKey"] as? String
        return result
    }
    
    private func getObjectsFromResponse(_ response: URLResponse?, _ data: Data?) {
        let json = RequestHelper.serializeJsonObjectWith(data: data!)
        let values = json["values"] as! [Any?]
        for item in values{
            
            self.lista.append(MapObject(item!))
        }
    }
    
    private func defineTable(){
        tabla.dataSource = self
        tabla.delegate = self
    }
}

