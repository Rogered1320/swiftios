import UIKit
import Foundation

public class RequestHelper{
    static func generateSession()->URLSession{
        let config = URLSessionConfiguration.default
        let userPasswordString = "jiralocaladmin:123456"
        let userPasswordData = userPasswordString.data(using: String.Encoding.utf8)
        let base64EncodedCredential = userPasswordData!.base64EncodedString()
        let authString = "Basic \(base64EncodedCredential)"
        config.httpAdditionalHeaders = ["Authorization" : authString]
        return URLSession(configuration: config)
    }
    
    static func serializeJsonObjectWith(data: Data) -> [String: Any]{
        var json = [String: Any]()
        do{
            json =
                try JSONSerialization.jsonObject(with: data,
                                                 options: JSONSerialization.ReadingOptions.mutableContainers) as! [String: Any]
        }
        catch {
            print("Error al procesar")
        }
        return json
    }
}

