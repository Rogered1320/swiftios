import UIKit
import Foundation

public class AsyncLoaderImag{
    
    static func load_image(image_url_string:String, view:UIImageView, table:UITableView)
    {
        
        let image_url: NSURL = NSURL(string: image_url_string)!
        let image_from_url_request: NSURLRequest = NSURLRequest(url: image_url as URL)
        
        NSURLConnection.sendAsynchronousRequest(
            image_from_url_request as URLRequest, queue: OperationQueue.main,
            completionHandler: {(response: URLResponse!, data: Data?, error: Error?) -> Void in
                if error == nil && data != nil {
                    print("entro")
                    //view.backgroundColor = UIColor.blue
                    view.image = UIImage(data: data!)
                }
                else{print("no entro")}
                
        })
        
    }
}

